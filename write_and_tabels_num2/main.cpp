#include <iostream>
#include <fstream>

using namespace std;

struct line{
	int a,b,c;
};

void rec(), view(), proc();

int main(){
	bool flagQuit = true;

	while(flagQuit){
		cout << "1. Record data in file F" << endl;
		cout << "2. View files" << endl;
		cout << "3. Process file" << endl;
		cout << "4. Quit" << endl;

		int mode;
		cin >> mode;

		switch (mode) {
		case 1:
			rec();
			break;
		case 2:
			view();
			break;
		case 3:
			proc();
			break;
		case 4:
			flagQuit = false;
			break;
		default:
			cout << "error" << endl;
			break;
		}
	}
	return 0;
}


void rec(){
	fstream f("f.txt", ios::binary|ios::out|ios::trunc);

	cout << "Enter amount line: ";
	int amountLines;
	cin >> amountLines;

	f.write((char *)&amountLines, sizeof(int));

	cout << "Enter (a, b, c): " << endl;
	line ln;

	for(int i=0; i<amountLines; i++){
		cin >> ln.a >> ln.b >> ln.c;
		f.write((char *)&ln, sizeof(line));
	}

	f.close();
}

void view(){
	fstream f("f.txt", ios::binary|ios::in);
	fstream g("g.txt", ios::binary|ios::in);

	int amountLinesF, amountLinesG;
	line lnF, lnG;
	int size;

	cout << "file - F: " << endl;

	f.seekg(0,ios::end);
	size = f.tellg();
	if(size != 0){
		f.seekg(0);
		f.read((char *) &amountLinesF, sizeof(int));

		for(int i=0; i<amountLinesF; i++){
			f.read((char *)&lnF, sizeof(line));
			cout << lnF.a << ends << lnF.b << ends << lnF.c << endl;
		}
		cout << endl;
	}
	else
		cout << "File is empty!" << endl << endl;

	cout << "file - G: " << endl;

	g.seekg(0,ios::end);
	size = g.tellg();
	if(size){
		g.seekg(0);
		g.read((char *) &amountLinesG, sizeof(int));

		for(int i=0; i<amountLinesG; i++){
			g.read((char *)&lnG, sizeof(line));
			cout << lnG.a << ends << lnG.b << ends << lnG.c << endl;
		}
		cout << endl;
	}
	else
		cout << "File is empty!" << endl << endl;

	f.close();
	g.close();
}

void proc(){
	fstream f("f.txt", ios::binary|ios::in);
	fstream g("g.txt", ios::binary|ios::out|ios::trunc);

	int amountLinesF, amountLinesG = 0;
	int size;

	f.seekg(0,ios::end);
	size = f.tellg();
	if(size != 0){
		f.seekg(0);
		f.read((char *) &amountLinesF, sizeof(int));

		line *lnF = new line[amountLinesF];

		for(int i=0; i<amountLinesF; i++)
			f.read((char *)&lnF[i], sizeof(line));

		bool *flags = new bool[amountLinesF];

		for(int i=0; i<amountLinesF; i++)
			flags[i] = true;

		for(int i=0; i<amountLinesF-1; i++){
			if(flags[i])
				for(int j=i+1; j<amountLinesF; j++)
					if((lnF[i].a == lnF[j].a) && (lnF[i].b == lnF[j].b) && (lnF[i].c == lnF[j].c))
						flags[j] = false;
		}

		for(int i=0; i<amountLinesF; i++)
			if(flags[i])
				amountLinesG++;

		g.write((char *)&amountLinesG, sizeof(int));

		for(int i=0; i<amountLinesF; i++)
			if(flags[i])
				g.write((char *)&lnF[i], sizeof(line));


	}
	else
		cout << "File F is empty!" << endl << endl;
}
