TEMPLATE = subdirs

SUBDIRS += \
    vectors_num43d \
    vectors_num34 \
    matrix_num4b \
    matrix_num31a \
    strings_25d \
    strings_13 \
    write_and_tabels_num2 \
    write_and_tabels_num8a \
    lists_num26 \
    lists_num2a \
    stacks_and_queues_num1b \
    stacks_and_queues_num10
