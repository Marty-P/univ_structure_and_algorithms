#ifndef QUEUES_H
#define QUEUES_H

#include <vector>
#include <iostream>

class Queues
{
	char *elements;
	int amountEl;
	int lastEl;
	int firstEl;

public:
	Queues(int x);
	~Queues();
	void clear();
	bool checkEmpty();
	void print();
	void add(char x);
	char remove();
};

#endif // QUEUES_H
