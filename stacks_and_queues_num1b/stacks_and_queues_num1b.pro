TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    queues.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    queues.h

