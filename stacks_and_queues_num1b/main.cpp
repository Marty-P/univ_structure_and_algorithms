#include <iostream>
#include "queues.h"

using namespace std;

int main()
{
	bool flagQuit = true;
	Queues queue(5);

	while(flagQuit){
		cout << "1. Clear" << endl;
		cout << "2. Check empty" << endl;
		cout << "3. Print" << endl;
		cout << "4. Add" << endl;
		cout << "5. Remove" << endl;

		int mode;
		cin >> mode;



		switch (mode) {
		case 1:
			queue.clear();
			break;
		case 2:
			if(queue.checkEmpty())
				cout << "queue is empty!" << endl;
			else
				cout << "queue is\'t empty!" << endl;

			break;
		case 3:
			queue.print();
			break;
		case 4:
			cout << "Enter symbol: ";
			char ch;
			cin >> ch;
			queue.add(ch);
			break;
		case 5:
			queue.remove();
			break;
		default:
			cout << "Error" << endl;
			flagQuit = false;
			break;
		}
	}

	return 0;
}

