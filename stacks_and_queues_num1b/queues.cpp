#include "queues.h"

Queues::Queues(int x)
{
	lastEl = -1;
	firstEl = -1;
	amountEl = x;
	elements = new char[amountEl];
	for(int i=0; i<x; i++)
		elements[i] = '_';
}

Queues::~Queues()
{

}

void Queues::clear()
{
	for(int i=0; i<amountEl; i++)
		elements[i] = '_';

	lastEl = -1;
	firstEl = -1;
}

bool Queues::checkEmpty()
{
	if(lastEl == -1)
		return true;
	else
		return false;
}

void Queues::print()
{
	std::cout << "lastEl = " << lastEl << std::endl;
	if(lastEl != -1){
		if(firstEl == 0){
			for(int i=0; i<=lastEl; i++)
				std::cout << elements[i] << " ";
			std::cout << std::endl;
		}
		else{
			int end = amountEl;
			int i = firstEl;
			bool flag = true;
			while(i<end){
				std::cout << elements[i] << " ";

				if(i == end-1 && flag){
					end = lastEl+1;
					i = -1;
					flag = false;
				}
				i++;
			}
			std::cout << std::endl;
		}
	}
	else
		std::cout << "Erorr: queue is empty" << std::endl;
}

void Queues::add(char x)
{
	if(firstEl == -1 && lastEl == -1){
		firstEl = 0;
		lastEl = 0;
	}
	else{
		if(firstEl == 0 && lastEl != amountEl-1)
			lastEl++;
		else
			if(firstEl == 0 && lastEl == amountEl-1){
				lastEl = 0;
				firstEl++;
			}
			else
				if(firstEl == amountEl-1){
					firstEl = 0;
					lastEl++;
				}
				else{
					firstEl++;
					lastEl++;
				}

	}
	elements[lastEl] = x;
}

char Queues::remove()
{
	char x;
	if(firstEl == -1)
		x = 0;
	else{
		x = elements[firstEl];
		elements[firstEl] = '_';
		if(firstEl != amountEl)
			firstEl++;
		else
			firstEl = 0;
	}

	return x;

}

