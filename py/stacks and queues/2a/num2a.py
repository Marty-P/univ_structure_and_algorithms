import stack_num2a

tn = stack_num2a.Stack(3)

flagQuit = 1

while flagQuit:
    print("1. Добавить елемент")
    print("2. Получить елемент")
    print("3. Очистить стек")
    print("4. Проверить на пустоту")
    print("5. Печать стека")
    print("0. Выход")

    mode = int(input("> "))
    
    if mode == 1:
        tn.addEl(input("> "))
    elif mode == 2:
        el = tn.getEl()
        print(el)
    elif mode == 3:
        print("Стек очищен!\n")
        tn.clear()
    elif mode == 4:
        if tn.checkEmpty():
            print("Стек пуст")
        else:
            print("В стеке есть элементы")
    elif mode == 5:
        tn.printStack()
    elif mode == 0:
        flagQuit = 0
    else:
        print("Ввод не корректен")


        
