class Stack:
    def __init__(self, x):
        self.elements = []
        self.lenStack = x
        self.amountEl = 0
        

    def addEl(self,x):
        if self.lenStack > self.amountEl:
            self.elements.append(x)
            self.amountEl += 1
        else:
            print("Ошибка: код - 2")

    def clear(self):
        self.elements.clear()
        self.amountEl = 0

    def getEl(self):
        self.amountEl -= 1
        el = self.elements.pop()
        return el

    def printStack(self):
        print(self.elements)

    def checkEmpty(self):
        return self.elements == []

