class turn:
    def __init__(self, x):
        self.elements = []
        self.lenTurn = x
        self.amountEl = 0
        

    def addEl(self,x):
        if self.lenTurn > self.amountEl:
            self.elements.append(x)
            self.amountEl += 1
        else:
            self.elements.pop(0)
            self.elements.append(x)

    def clear(self):
        self.elements.clear()
        self.amountEl = 0

    def getEl(self):
        el = self.elements.pop(0)
        self.amountEl -= 1
        return el

    def printTurn(self):
        print(self.elements)

    def checkEmpty(self):
        return self.elements == []

