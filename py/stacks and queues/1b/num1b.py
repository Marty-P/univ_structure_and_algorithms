import turn_num1b

tn = turn_num1b.turn(3)

flagQuit = 1

while flagQuit:
    print("1. Добавить елемент")
    print("2. Получить елемент")
    print("3. Очистить очередь")
    print("4. Проверить на пустоту")
    print("5. Печать очереди")
    print("0. Выход")

    mode = int(input("> "))
    
    if mode == 1:
        tn.addEl(input("> "))
    elif mode == 2:
        el = tn.getEl()
        print(el)
    elif mode == 3:
        print("Очередь очищена!\n")
        tn.clear()
    elif mode == 4:
        if tn.checkEmpty():
            print("Очередь пуста")
        else:
            print("В очереди есть элементы")
    elif mode == 5:
        tn.printTurn()
    elif mode == 0:
        flagQuit = 0
    else:
        print("Ввод не корректен")


        
