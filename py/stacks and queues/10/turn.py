class Turn:
    def __init__(self):
        self.elements = []

    def addEl(self,x):
        self.elements.append(x)

    def getEl(self):
        el = self.elements.pop(0)
        return el

    def checkEmpty(self):
        return self.elements == []
