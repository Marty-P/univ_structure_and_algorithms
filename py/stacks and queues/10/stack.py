class Stack:
    def __init__(self):
        self.elements = []
        
    def addEl(self,x):
        self.elements.append(x)

    def getEl(self):
        el = self.elements.pop()
        return el

    def checkEmpty(self):
        return self.elements == []
