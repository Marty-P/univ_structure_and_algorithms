from tree import Tree

def result(tree):
    if tree.left == None and tree.right == None:
        return float(str(tree))
    else:
        if str(tree) == "+":
            return result(tree.left) + result(tree.right)
        elif str(tree) == "-":
            return result(tree.left) - result(tree.right)
        elif str(tree) == "*":
            return result(tree.left) * result(tree.right)
        elif str(tree) == "/":
            return result(tree.left) / result(tree.right)


tr = Tree("*", Tree(5), Tree("+",Tree(3),Tree(8)))

print(result(tr))
