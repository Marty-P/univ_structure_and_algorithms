from tree import Tree

def printFoliage(tree):
    if tree.left == None and tree.right == None:
        print(tree)
    else:
        printFoliage(tree.left)
        printFoliage(tree.right)

tr = Tree('+',Tree(5),Tree(6,Tree(10), Tree(8,Tree(90),Tree(7))))

printFoliage(tr)
