#include <iostream>
#include <cstring>

using namespace std;

int main()
{
	string src, result;
	src = "child of light";
	result = "";
	cout << "source: " << src << endl;
	cout << "result: ";

	bool flag = true;
	int pos=0;


	while(flag){
		pos = strcspn(src.c_str(), " ");
		if(pos){
			string word = src.substr(0,pos);
			if ( word == "child" )
				result += "children ";
			else
				result += word + " ";

			src.erase(0,pos+1);
		}
		else
			flag = false;

	}

	cout << result;

    return 0;
}

