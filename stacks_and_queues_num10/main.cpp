#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

int main()
{
	fstream file("f.txt", ios::in);
	file.seekg (0, std::ios::end);
	int len = file.tellg();
	file.seekg (0);
	int amountP = 0;

	char text[len];
	for(int i=0; i<len; i++){
		text[i] = file.get();
		if(text[i] == '(' || text[i] == ')')
			amountP++;
	}
	amountP /= 2;

	int **index = new int*[amountP];
	for(int i=0; i<amountP; i++)
		index[i] = new int[2];

	for(int i=0; i<amountP; i++)
		for(int j=0; j<2; j++)
			index[i][j] = -1;

	//поиск индексов скобок
	int j=0;
	for(int i=0; i<len; i++){
		if(text[i] == '('){
			while(index[j][0] != -1)
				j++;
			index[j][0] = i+1;
		}
		else if(text[i] == ')'){
			while(index[j][1] != -1)
				j--;
			index[j][1] = i+1;
		}
	}

	int k = amountP-1;
	while(k>0){
		int id = 0;
		for(int i=0; i<=k; i++)
			if(index[i][0] > index[id][0])
				id = i;

		int pO = index[id][0];
		int pC = index[id][1];
		index[id][0] = index[k][0];
		index[id][1] = index[k][1];
		index[k][0] = pO;
		index[k][1] = pC;
		k--;
	}

	for(int i=0; i<amountP; i++)
		cout << index[i][0] << " " << index[i][1] << endl;


	return 0;
}

