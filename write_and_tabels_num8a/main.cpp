#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;

struct book{
	char autor[30];
	char title[30];
	int date;
};

void rec(), view(), search();

int main()
{
	bool flagQuit = true;

	while(flagQuit){

		cout << "1. Rec data" << endl;
		cout << "2. View data" << endl;
		cout << "3. Search data" << endl;
		cout << "4. Quit" << endl;

		int mode;
		cin >> mode;

		switch (mode) {
		case 1:
			rec();
			break;
		case 2:
			view();
			break;
		case 3:
			search();
			break;
		case 4:
			flagQuit = false;
			break;
		default:
			break;
		}

	}
}

void rec(){
	fstream file("f.txt", ios::binary|ios::out|ios::trunc);

	cout << "Enter amount books: ";
	int amount;
	cin >> amount;

	file.write((char *)&amount, sizeof(int));

	cout << "Enter informations about book (autor, title, date): " << endl;

	book books;

	for(int i=0; i<amount; i++){
		cin >> books.autor >> books.title >> books.date;
		file.write((char *)&books,sizeof(book));
	}
}

void view(){
	fstream file("f.txt", ios::binary|ios::in);

	int amount;
	file.read((char *)&amount, sizeof(int));

	book books;

	for(int i=0; i<amount; i++){
		file.read((char *)&books, sizeof(book));
		cout << books.autor << ends << books.title << ends << books.date << endl;

	}
}

void search(){
	string autor;
	cout << "Enter autor: ";
	cin >> autor;

	fstream file("f.txt", ios::binary|ios::in);

	int amount;
	file.read((char *)&amount, sizeof(int));

	book books;

	for(int i=0; i<amount; i++){
		file.read((char *)&books, sizeof(book));
		if( !strcmp(autor.c_str(), books.autor) && books.date >= 1960 )
			cout << books.title << endl;

	}
}
