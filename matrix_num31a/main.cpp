#include <iostream>
#define N 3

using namespace std;

int main()
{
	double matrix[N][N];

	for(int i=0; i<N; i++)
		for(int j=0; j<N; j++){
			cout << "[" << i+1 << "][" << j+1 << "]: ";
			cin >> matrix[i][j];
		}

	for(int i=0; i<N; i++){
		for(int j=0; j<N; j++)
			cout << matrix[i][j] << "  ";
		cout << endl;
	}


	for(int i=0; i<N; i++){
		double summ = 0;
		if(matrix[i][i] < 0){
			for(int j=0; j<N; j++)
				summ += matrix[i][j];

			cout << "summ " << i+1 << ": " << summ << endl;
		}
	}

	return 0;
}

