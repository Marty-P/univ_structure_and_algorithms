#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main()
{
	int n;
	cout << "(n): ";
	cin >> n;

	cout.setf(ios::fixed);
	double **a = new double*[n];
	for(int i=0; i<n; i++)
		a[i] = new double[n];

	for(int i=0; i<n; i++)
		for(int j=0; j<n; j++)
			if(i<j)
				a[i][j] = sin(i+j);
			else if(i == j)
				a[i][j] = 1;
			else
				a[i][j] = asin((i+j)/(2*i + 3*j));

	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++)
			cout << setprecision(2) << a[i][j] << "  ";

		cout << "\n";
	}

	return 0;
}

