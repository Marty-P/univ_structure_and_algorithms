#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	int n;
	cout << "(n): ";
	cin >> n;

	double *y = new double[n];
	cout << "(y): ";
	for(int i=0; i<n; i++)
		cin >> y[i];

	double result = 0;

	for(int i=0; i<n; i++)
		if( fabs(y[i])<1 )
			result += y[i];
		else
			result += 1/y[i];

	cout << endl << "result = " << result << endl;
	return 0;
}

