#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	int n;
	cout << "(n): ";
	cin >> n;

	double *a = new double[n];

	for(int i=0; i<n; i++)
		a[i] = (i-1)/(i+1) + sin(pow(i-1,3.)/(i+1));

	double min;

//	for(int i=0; i<n; i++)
//		cout << a[i] << "   ";

	cout << endl;

	for(int i=0; i<n; i++)
		if(a[i]>0){
			min = a[i];
			cout << a[i] << "   ";
		}

	for(int i=0; i<n; i++)
		if(a[i]>0 && a[i]<min)
			min = a[i];

	cout << endl <<  "min: " << min << endl;


	return 0;
}

